//
//  ImagePostCell.swift
//  FeedDemo-MVVM
//
//  Created by HARSHIT on 16/11/21.
//

import UIKit

class ImagePostCell: UITableViewCell {

    var titleLbl = UILabel()
    var customImageView = UIImageView()
    var commentsNoLbl = UILabel()
    var scoreLbl = UILabel()

    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                customImageView.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                customImageView.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add the UI components
        titleLbl.translatesAutoresizingMaskIntoConstraints = false
        titleLbl.numberOfLines = 0
        contentView.addSubview(titleLbl)
        
        customImageView.translatesAutoresizingMaskIntoConstraints = false
        customImageView.contentMode = .scaleAspectFit
        contentView.addSubview(customImageView)
        
        commentsNoLbl.translatesAutoresizingMaskIntoConstraints = false
        commentsNoLbl.numberOfLines = 0
        contentView.addSubview(commentsNoLbl)
        
        scoreLbl.translatesAutoresizingMaskIntoConstraints = false
        scoreLbl.numberOfLines = 0
        contentView.addSubview(scoreLbl)
        
        NSLayoutConstraint.activate([
            titleLbl.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            titleLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            titleLbl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            
            customImageView.topAnchor.constraint(equalTo: titleLbl.bottomAnchor, constant: 10.0),
            customImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            customImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            
            commentsNoLbl.topAnchor.constraint(equalTo: customImageView.bottomAnchor, constant: 10.0),
            commentsNoLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            commentsNoLbl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            
            scoreLbl.topAnchor.constraint(equalTo: commentsNoLbl.bottomAnchor, constant: 10.0),
            scoreLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            scoreLbl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            scoreLbl.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10.0),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
    func setCustomImageHeight(_ imageHeight: Int, imageWidth: Int) {
        if imageWidth != 0 && imageHeight != 0 {
            let aspect = Float(imageWidth) / Float(imageHeight)
            let constraint = NSLayoutConstraint(item: customImageView, attribute: .width, relatedBy: .equal, toItem: customImageView, attribute: .height, multiplier: CGFloat(aspect), constant: 0.0)
            constraint.priority = UILayoutPriority.defaultHigh
            aspectConstraint = constraint
        } else {
            let constraint = NSLayoutConstraint(item: customImageView, attribute: .width, relatedBy: .equal, toItem: customImageView, attribute: .height, multiplier: 1.0, constant: 0.0)
            aspectConstraint = constraint
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
