//
//  FeedListModel.swift
//  FeedDemo-MVVM
//
//  Created by HARSHIT on 16/11/21.
//

import Foundation

// MARK: - BaseResultModel

struct ResultModel : Codable {
    
    let kind : String?
    let data : Datum?
    
    enum CodingKeys: String, CodingKey {
        case kind = "kind"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(Datum.self, forKey: .data)
        kind = try values.decodeIfPresent(String.self, forKey: .kind)
    }
}

struct Datum : Codable {
    
    let after : String?
    let before : String?
    let children : [Child]?
    let dist : Int?
    
    enum CodingKeys: String, CodingKey {
        case after = "after"
        case before = "before"
        case children = "children"
        case dist = "dist"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        after = try values.decodeIfPresent(String.self, forKey: .after)
        before = try values.decodeIfPresent(String.self, forKey: .before)
        children = try values.decodeIfPresent([Child].self, forKey: .children)
        dist = try values.decodeIfPresent(Int.self, forKey: .dist)
    }
}


struct Child : Codable {

    let childData : ChildData?
    let kind : String?

    enum CodingKeys: String, CodingKey {
        case childData = "data"
        case kind = "kind"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        childData = try values.decodeIfPresent(ChildData.self, forKey: .childData)
        kind = try values.decodeIfPresent(String.self, forKey: .kind)
    }
}

struct ChildData : Codable {
    
    let thumbnail : String?
    let thumbnailHeight : Int?
    let thumbnailWidth : Int?
    let title : String?
    let created : Int?
    let id : String?
    let score : Int?
    let numComments : Int?
    
    enum CodingKeys: String, CodingKey {
        case thumbnail = "thumbnail"
        case thumbnailHeight = "thumbnail_height"
        case thumbnailWidth = "thumbnail_width"
        case title = "title"
        case created = "created"
        case id = "id"
        case score = "score"
        case numComments = "num_comments"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        created = try values.decodeIfPresent(Int.self, forKey: .created)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        numComments = try values.decodeIfPresent(Int.self, forKey: .numComments)
        score = try values.decodeIfPresent(Int.self, forKey: .score)
        thumbnail = try values.decodeIfPresent(String.self, forKey: .thumbnail)
        thumbnailHeight = try values.decodeIfPresent(Int.self, forKey: .thumbnailHeight)
        thumbnailWidth = try values.decodeIfPresent(Int.self, forKey: .thumbnailWidth)
        title = try values.decodeIfPresent(String.self, forKey: .title)
    }
}
