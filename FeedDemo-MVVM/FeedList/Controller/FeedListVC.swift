//
//  FeedListVC.swift
//  FeedDemo-MVVM
//
//  Created by HARSHIT on 16/11/21.
//

import UIKit

class FeedListVC: UIViewController {
    
    //MARK: Variables
    lazy private var feedListView: FeedListView = { [unowned self] in
        return self.view as! FeedListView
    }()
    lazy private var viewModel: FeedListViewModel  = {
        return FeedListViewModel(withVC: self, view: feedListView)
    }()

    // Variabls
    var onBack: (() -> Void)?
    var onContinue: (() -> Void)?

    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.getFeedDataAPI_Call()
    }
    
    func setupUI() {
        self.setUpTableView()
    }
 
    private func setUpTableView() {
        self.feedListView.tableView.register(ImagePostCell.self, forCellReuseIdentifier: "ImagePostCell")
        //self.feedListView.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        
        self.feedListView.tableView.rowHeight = UITableView.automaticDimension
        self.feedListView.tableView.estimatedRowHeight = 20
        self.feedListView.tableView.delegate = viewModel
        self.feedListView.tableView.dataSource = viewModel
        self.feedListView.tableView.contentInsetAdjustmentBehavior = .never
    }
    
    //MARK: API Callback
    func getFeedDataAPI_Call() {
        self.viewModel.apiCall_getFeedData()
    }
    
}
