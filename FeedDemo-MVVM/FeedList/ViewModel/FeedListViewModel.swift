//
//  FeedListViewModel.swift
//  FeedDemo-MVVM
//
//  Created by HARSHIT on 16/11/21.
//

import Foundation
import SwiftMessageBar
import SDWebImage

class FeedListViewModel: NSObject {
    
    fileprivate weak var viewController:FeedListVC!
    fileprivate weak var view:FeedListView!
    var children_arr = [Child]()

    init(withVC:FeedListVC, view:FeedListView) {
        self.viewController = withVC
        self.view = view
    }
    
    func apiCall_getFeedData() {
        ServiceController.serviceGET([:], path: String.BaseURL(.getFeed)) { responseObj, data in
            do {
                if let children = try JSONDecoder().decode(ResultModel.self, from: data).data?.children, children.count > 0 {
                    debugPrint(children)
                    self.children_arr.append(contentsOf: children)
                    self.view.tableView.reloadData()
                }
            } catch {
                SwiftMessageBar.showMessage(withTitle: "Error", message: error.localizedDescription, type: .error)
                debugPrint(error)
            }
        } failure: { error in
            SwiftMessageBar.showMessage(withTitle: "Error", message: error?.localizedDescription, type: .error)
        }
    }
}

extension FeedListViewModel: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return children_arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImagePostCell", for: indexPath) as! ImagePostCell
        let obj = children_arr[indexPath.row]
        cell.setCustomImageHeight(obj.childData?.thumbnailHeight ?? 0, imageWidth: obj.childData?.thumbnailWidth ?? 0)
        cell.customImageView.sd_setImage(with: URL(string: obj.childData?.thumbnail ?? ""), placeholderImage: UIImage(named: "no_image_available"))
        cell.titleLbl.text = obj.childData?.title ?? "N/A"
        cell.commentsNoLbl.text = "Comments: \(obj.childData?.numComments ?? 0)"
        cell.scoreLbl.text = "Score: \(obj.childData?.score ?? 0)"
        return cell
    }
}

extension FeedListViewModel: UITableViewDelegate {
    
}
