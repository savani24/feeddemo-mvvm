//
//  ServiceController.swift
//  FeedDemo-MVVM
//
//  Created by HARSHIT on 16/11/21.
//

import UIKit

class ServiceController: NSObject
{
    static let serviceTimeOutInterval = 100.0
    
    class func getHeaders() -> [String:String] {
        /*if let userAccess = AccessTokenModel.fromUserdefaults(), ((userAccess.access_token ?? "") != "") {
            return ["Content-Type": "application/json",
                    "cache-control": "no-cache",
                    "Accept": "application/json",
                    "Authorization":"\(userAccess.token_type!) \(userAccess.access_token!)"]
        }
        else
        {
            return [ "Content-Type": "application/json",
                     "cache-control": "no-cache",
                     "Accept": "application/json"]
        }*/
        return [ "Content-Type": "application/json",
                 "cache-control": "no-cache",
                 "Accept": "application/json"]
    }
    
    class func serviceGET(_ parameters:NSDictionary, path:String, withSuccess: @escaping ((_ responseObj: AnyObject?, _ data: Data) -> Void), failure:@escaping ((_ error: Error?) -> Void))
    {
        show_HUD()
        DispatchQueue.global(qos: .userInitiated).async
        {
            var urlQuery = path
            if parameters.allKeys.count > 0 {
                var components = URLComponents()
                components.queryItems = parameters.map {
                    URLQueryItem(name: $0 as! String, value: $1 as? String)
                }
                urlQuery = path.appending(components.url?.absoluteString ?? "")
            }
            let request = NSMutableURLRequest(url: URL(string:urlQuery)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Double.infinity)
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = ServiceController.getHeaders()
                        
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil)
                {
                    DispatchQueue.main.async {
                        failure(error)
                    }
                } else {
                    do {
                        let jsonDictionary:NSDictionary  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if let error = ServerErrorCheck.checkForError(jsonDictionary) {
                            DispatchQueue.main.async {
                                failure(error)
                            }
                        } else {
                            DispatchQueue.main.async {
                                withSuccess(jsonDictionary, data!)
                            }
                        }
                    } catch {
                        print(error.localizedDescription)
                        DispatchQueue.main.async {
                            failure(error)
                        }
                    }
                }
                hide_HUD()
            })
            dataTask.resume()
        }
    }
    
    class func servicePOST(_ parameters:NSDictionary, path:String, withSuccess: @escaping ((_ responseObj: AnyObject?, _ data: Data) -> Void), failure:@escaping ((_ error: Error?) -> Void))
    {
        show_HUD()
        DispatchQueue.global(qos: .userInitiated).async
        {
            let request = NSMutableURLRequest(url: URL(string:path)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: serviceTimeOutInterval)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = ServiceController.getHeaders()
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
                failure(error)
            }
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil)
                {
                    DispatchQueue.main.async {
                        failure(error)
                    }
                } else {
                    do {
                        let jsonDictionary:NSDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if let error = ServerErrorCheck.checkForError(jsonDictionary) {
                            DispatchQueue.main.async {
                                failure(error)
                            }
                        } else {
                            DispatchQueue.main.async {
                                withSuccess(jsonDictionary, data!)
                            }
                        }
                    } catch {
                        print(error.localizedDescription)
                        DispatchQueue.main.async {
                            failure(error)
                        }
                    }
                }
                hide_HUD()
            })
            dataTask.resume()
        }
    }
    
    class func serviceImagePOST(parameter:NSDictionary,path:String,image:UIImage,isImage:Bool,withSuccess: @escaping ((_ responseObj: AnyObject?) -> Void), failure:@escaping ((_ error: Error?) -> Void))
    {
        show_HUD()
        DispatchQueue.global(qos: .userInitiated).async {
            
            let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
            /*guard let userAccess = AccessTokenModel.fromUserdefaults(), ((userAccess.access_token ?? "") != "") else {
                return
            }
            let headers = [
                "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "Content-Type": "application/x-www-form-urlencoded",
                "Cache-Control": "no-cache",
                "Postman-Token": "3452ea41-abaa-3c0b-8b16-f876e0f301e8",
                "Authorization":"\(userAccess.token_type!) \(userAccess.access_token!)"
            ]
            */
            
            let request = NSMutableURLRequest(url: URL(string:path)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: serviceTimeOutInterval)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = [:]//headers
            
            let body = NSMutableData();
            for (key, value) in parameter
            {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
            if(isImage)
            {
                let filename = "\(UUID().uuidString).jpg"
                let mimetype = "image/jpg"
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"avatar\"; filename=\"\(filename)\"\r\n")
                body.appendString("Content-Type: \(mimetype)\r\n\r\n")
                body.append((image.jpegData(compressionQuality: 0.5))!)
                body.appendString("\r\n")
                body.appendString("--\(boundary)--\r\n")
            }
            request.httpBody = body as Data
            
            let datastring = NSString(data: body as Data, encoding: String.Encoding.utf8.rawValue)
            print(datastring as Any)
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                hide_HUD()
                if (error != nil)
                {
                    DispatchQueue.main.async {
                        failure(error)
                    }
                } else {
                    do {
                        let jsonDictionary:NSDictionary;
                        jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        DispatchQueue.main.async {
                            withSuccess(jsonDictionary)
                        }
                    } catch {
                        print(error.localizedDescription)
                        DispatchQueue.main.async {
                            failure(error)
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
}

extension NSMutableData
{
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

class ServerErrorCheck: NSObject {
    class func checkForError(_ object:NSDictionary) -> Error? {
        if let error_obj = object["errors"] as? NSDictionary {
            if let errorKeys = error_obj.allKeys as? [String], errorKeys.count > 0, let firstErrorKey = errorKeys.first {
                if let errorMessage = error_obj[firstErrorKey] as? NSArray, errorMessage.count > 0 {
                    return ValidationError().getError(errorMessage[0] as? String ?? "Unknow server error!")
                } else {
                    return ValidationError().getError("Unknow server error!")
                }
            } else {
                return ValidationError().getError("Unknow server error!")
            }
        }
        return nil
    }
}
