//
//  SwiftExtensionClass.swift
//  FeedDemo-MVVM
//
//  Created by HARSHIT on 16/11/21.
//

import UIKit
import Foundation
import JTProgressHUD

class ValidationError: Error {
    
    func getError(_ message: String) -> NSError {
        let error = NSError(domain: "com.App.FeedDemo-MVVM", code: 101, userInfo: [
            NSLocalizedDescriptionKey: message
        ])
        return error
    }
}

extension UITextField {
    
    func validated() throws -> String {
        guard !(self.text?.isEmpty ?? true) else {
            throw ValidationError().getError("Required field: " + (self.placeholder ?? ""))
        }
        return self.text ?? ""
    }
}

extension String {
    
    func maskedEmail() -> String {
        let regex = try! NSRegularExpression(pattern: "(?<=.)[^@](?=[^@]*?@)|(?:(?<=@.)|(?!^)\\G(?=[^@]*$)).(?=.*[^@]\\.)", options: NSRegularExpression.Options.caseInsensitive)
        let range = NSMakeRange(0, self.count)
        let maskEmail = regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: "*")
        return maskEmail
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func validatedEmail() throws -> String {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.count)) == nil {
                throw ValidationError().getError("Invalid e-mail Address")
            }
        } catch {
            throw ValidationError().getError("Invalid e-mail Address")
        }
        return self
    }
    
    func validatedPassword() throws -> String {
        guard self != "" else {throw ValidationError().getError("Password is Required")}
        guard self.count >= 6 else { throw ValidationError().getError("Password must have at least 6 characters") }
        
        do {
            if try NSRegularExpression(pattern: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$",  options: .caseInsensitive).firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.count)) == nil {
                throw ValidationError().getError("Password must be more than 6 characters, with at least one character and one numeric character")
            }
        } catch {
            throw ValidationError().getError("Password must be more than 6 characters, with at least one character and one numeric character")
        }
        return self
    }
    
    func validatedConfirmPassword(_ password:String?) throws -> String {
        guard self != "" else { throw ValidationError().getError("Confirm Password is Required") }
        guard self.count >= 6 else { throw ValidationError().getError("Confirm Password must have at least 6 characters") }
        guard self == password else { throw ValidationError().getError("Confirm Password must match with password") }
        return self
    }
    
    func validatedAge() throws -> String {
        guard self.count > 0 else { throw ValidationError().getError("Age is required") }
        guard let age = Int(self) else { throw ValidationError().getError("Age must be a number!") }
        guard self.count < 3 else { throw ValidationError().getError("Invalid age number!") }
        guard age >= 18 else {throw ValidationError().getError("You have to be over 18 years old to user our app :)") }
        return self
    }

    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        return label.frame.height
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension String {
    
    //Converts String to Int
    public func toInt() -> Int! {
        if let num = NumberFormatter().number(from: self) {
            return num.intValue
        } else {
            return 0
        }
    }
    
    //Converts String to Double
    public func toDouble() -> Double! {
        if let num = NumberFormatter().number(from: self) {
            return num.doubleValue
        } else {
            return 0.0
        }
    }
    
    /// EZSE: Converts String to Float
    public func toFloat() -> Float! {
        if let num = NumberFormatter().number(from: self) {
            return num.floatValue
        } else {
            return 0.0
        }
    }
    
    //Converts String to Bool
    public func toBool() -> Bool! {
        return (self as NSString).boolValue
    }
}


extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

extension UIView {
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension UIViewController {
    
    var App_Delegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func configureChildViewController(childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        addChild(childController)
        holderView?.addSubview(childController.view)
        constrainViewEqual(holderView: holderView!, view: childController.view)
        childController.didMove(toParent: self)
        childController.willMove(toParent: self)
    }
    
    func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func getIPAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" || name == "pdp_ip0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        return address
    }
}

enum UserDefaultsKeys : String {
    case isLoggedIn
    case userID
}

extension UserDefaults {
    
    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    //MARK: Save User Data
    func setUserID(value: Int){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getUserID() -> Int{
        return integer(forKey: UserDefaultsKeys.userID.rawValue)
    }
}

func show_HUD() {
    JTProgressHUD.show()
}

func hide_HUD() {
    JTProgressHUD.hide()
}
