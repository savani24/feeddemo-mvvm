//
//  Constant.swift
//  FeedDemo-MVVM
//
//  Created by HARSHIT on 16/11/21.
//

import Foundation
import UIKit

enum Storyboard {
    case Main
    
    var Instance: UIStoryboard {
        switch self {
        case .Main:
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
}

extension String
{
    static func BaseURL(_ path:API_EndPoint) -> String
    {
        return "http://www.reddit.com/\(path.rawValue)"
    }
    
    static func getImageURL(_ path:String) -> String
    {
        return "http://www.reddit.com/images/\(path)"
    }
    
    static func LocalURL(_ path:API_EndPoint) -> String
    {
        return "http://localhost:8080/api/\(path)"
    }
}

enum API_EndPoint: String {
    case getFeed = ".json"
}

